#!/usr/bin/env ruby
# Problem 3
#
# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?
#
# Solution: I couldn't "brute-force" this problem the way that I did in
# Problem 2.
#
# Generating a list of primes takes too much memory, especially when
# using a hand-written implementation of the [Sieve of
# Eratosthenes][SOE].  Although I later discovered the `Prime` class in
# the Ruby Standard Library, which will provide an Enumerator for prime
# numbers, searching this list involves testing ~500,000 values (based
# on values of the [prime counting function][PCF] for
# sqrt(600851475143).
#
# Looking for factors, then testing if they are prime, was my second
# approach.  I covered more ground this way, but still couldn't come up
# with an answer in a sensible amount of time.  
#
# The best option here was to use a "trial division" approach.  Even
# with an inefficient way finding divisors (2..sqrt(n)), this method
# produced the correct answer with a minimum of fuss.
#
# [SOE]: http://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
# [PCF]: http://en.wikipedia.org/wiki/Prime-counting_function

## My original solution:
class Integer
	def prime_factors
		value = self
		factors = []
		# By starting at 2, we only get prime factors even though every
		# number is tested.
		(2..Math.sqrt(self)).each do |n| 
			while value % n == 0 do
				factors << n
				value /= n
			end

		break if value == 1
		end
		factors
	end
end

number = 600851475143
puts number.prime_factors.max

## A shorter solution using the Ruby Standard Library:
# require 'prime'
# number = 600851475143
# puts number.prime_division.collect{|x| x[0]}.max
