#!/usr/bin/env ruby
# Problem 7
#
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can
# see that the 6th prime is 13.
#
# What is the 10,001st prime number?
#
# Solution: Now that I know about the 'prime' module in the Ruby
# Standard Library, I can't resist ...
require 'prime'
Prime.each.each_with_index do |prime, index|
  if index+1 == 10001
    puts prime
    break
  end
end

# However, based on my work in Problem 3, and our knowledge that the
# prime itself is relatively small, the solution to this problem should
# be tractable "by hand", using a "trial division" approach:
primes = []
value  = 2
index  = 1

while index < 10002 do
  # I'll still steal #prime? from Prime ... but I *could* have written
  # it myself ...
  if value.prime?
    primes << value
    index += 1
  end
  value += 1
end
# It's not *instant*, but still fast.
puts primes.last
